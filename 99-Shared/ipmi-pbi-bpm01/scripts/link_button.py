# This script is to generate list of available links in ascending order each time, when the window is opened
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:  
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
	from org.phoebus.framework.macros import Macros
    else:
	from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil
    ConsoleUtil.writeInfo("Executing in BOY")

from org.csstudio.display.builder.model import WidgetFactory
from org.csstudio.display.builder.model.persist import ModelWriter
from org.csstudio.display.builder.model.properties import ActionInfos
from org.csstudio.display.builder.model.properties import OpenDisplayActionInfo
from org.csstudio.display.builder.model.properties.OpenDisplayActionInfo import Target
from org.csstudio.display.builder.model.widgets import ActionButtonWidget
from java.util import Arrays


# get display reference
def getDisplay():
	display = widget.getDisplayModel()
	return display
	

# get display reference
display = getDisplay()
# get macros to generate PVs names
macro = display.getEffectiveMacros()
# macro constructor
macros = Macros()
# get AMC macro value
macro1 = macro.getValue('AMC')
# array for button actions
open_info=[]
# prepare array with AMCs indexes
macro_array = []
for i in range(1, 13):
	macro_array.append(i)

# remove value of the current AMC
macro_array.remove(int(macro1))
# generate list of windows to be available
for idx, val in enumerate(macro_array):
	macros_list = Macros()
	macros_list.add('AMC', str(val))
	open_info.append(OpenDisplayActionInfo("AMC" + str(val), '../links/Links.bob', macros_list, Target.REPLACE))

# Load actions to button
info_list = Arrays.asList(open_info )
actions_info = ActionInfos(info_list)
widget.propActions().setValue(actions_info)