# This script is check panels version
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


ver = PVUtil.getString(pvs[1])
ioc_ver = PVUtil.getString(pvs[0])

ScriptUtil.getLogger().severe(ver)
ScriptUtil.getLogger().severe(ioc_ver)

if ver != ioc_ver:
	PVUtil.writePV("loc://BlinkingVersion", 1, 2000)
else:
	ScriptUtil.getLogger().severe("dupa")

	PVUtil.writePV("loc://BlinkingVersion", 0, 2000)